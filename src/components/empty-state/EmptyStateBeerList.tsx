import React from 'react';
import styled from 'styled-components';

import { CenterContent } from '../styles'

const Layout = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 30px;
  align-items: center;
`

const EmptyState: React.FC = () => {
  return (
    <CenterContent>
      <Layout>
        <h2>No Results Found. Try selecting something else or refresh the page.</h2>
      </Layout>
    </CenterContent>
  )
}

export default EmptyState;