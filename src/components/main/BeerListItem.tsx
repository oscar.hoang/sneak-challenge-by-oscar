import React from 'react';

import Image from '../utilities/Image';
import Favorite from '../utilities/Favorite';

import { OverlapContentContainer, Card } from '../styles'

interface Props {
  beer: any
  nextPage: Function
}

const BeerListItem: React.FC<Props> = ({ beer, nextPage }) =>
  <Card>
    <OverlapContentContainer>
      <Favorite
        beerID={beer.id}
        favorited={beer.FavoriteBeers.length > 0 ? true : false} />
      <Image
        beer={beer}
        isClickable={true}
        nextPage={nextPage} />
    </OverlapContentContainer>
    <h3>{beer.title}</h3>
  </Card>

export default BeerListItem;