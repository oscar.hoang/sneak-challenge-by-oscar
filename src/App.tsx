import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import Homepage from "./components/main/Homepage";

const client = new ApolloClient({
  uri: 'http://localhost:3001/',
});

const App: React.FC = () => {
  return (
    <ApolloProvider client={client}>
      <Homepage />
    </ApolloProvider>
  );
}

export default App;
