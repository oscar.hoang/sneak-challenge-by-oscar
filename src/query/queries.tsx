import gql from 'graphql-tag';

export const GET_ALL_BEERS_FILTERED_QUERY = gql`
  query getBeerFeed (
    $page: Int
    $perPage: Int
    $sortField: String
    $sortOrder: String
    $style_id: ID
    $country_id: ID
    ) {
    allBeers(
      page: $page
      perPage: $perPage
      sortField: $sortField
      sortOrder: $sortOrder
      filter: {
        style_id: $style_id
        country_id: $country_id
      }
    ) {
      id
      title
      description
      style_id
      country_id
      image_path
      FavoriteBeers {
        id
      }
    }
  }
`

export const GET_BEER_QUERY = gql`
  query getSelectedBeerByID ($id: ID!) {
    Beer(id: $id) {
      id
      title
      description
      image_path
      Style {
        id
        name
      }
      Country {
        name
      }
      FavoriteBeers {
        id
      }
    }
  }
`

export const GET_FAVORITES_COUNT_QUERY = gql`
  query getFavoritesCount {
    _allFavoriteBeersMeta {
      count
    }
  }
`

export const GET_ALL_STYLE_COUNTRY_QUERY = gql`
  {
    allStyles{
      id
      name
      Beers {
        id
        title
        description
        style_id
        country_id
        image_path
      }
    }
    allCountries{
      id
      name
      Beers {
        id
        title
        description
        style_id
        country_id
        image_path
      }
    }

  }
`