import React, { useState, useEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import styled from 'styled-components';

import { GET_ALL_BEERS_FILTERED_QUERY, GET_FAVORITES_COUNT_QUERY } from '../../query/queries';
import { CREATE_FAVORITE_MUTATION, REMOVE_FAVORITE_MUTATION } from '../../query/mutations';

const HeartSymbol = styled.span<{ favorited: boolean }>`
  display: block;
  height: 42px;
  margin-left: auto;
  text-align: center;
  width: 42px;
  position:absolute;
  top: 0;
  right: 8px;
  left: 0;
  bottom: 0;
  cursor: pointer;
  transition: all 0.3s ease-in-out;

  @media(hover: hover) and (pointer: fine) {
    &:hover {
      transform: scale(1.25);
      z-index: 300;
    }
  }
  &::before{
    color: ${props => props.favorited ? "#f3166e" : "gray"};
    content: "❤";
    font-size: 42px;
    position: relative;
    top: 0;
    @media (max-width: 768px) {
      font-size: 72px;
    }
  }
  @media (max-width: 768px) {
    height: 72px;
    width: 72px;
    right: 16px;
  }
`

interface Props {
  beerID: string
  favorited: boolean
}

const AddFavorite: React.FC<Props> = ({ beerID, favorited }) => {
  const [shouldFetch, setShouldFetch] = useState<boolean>(false);

  const [createFavorite] = useMutation(CREATE_FAVORITE_MUTATION, {
    variables: { id: beerID, beer_id: beerID },
    refetchQueries: [{ query: GET_ALL_BEERS_FILTERED_QUERY }, { query: GET_FAVORITES_COUNT_QUERY }]
  })

  const [removeFavorite] = useMutation(REMOVE_FAVORITE_MUTATION, {
      variables: { id: beerID },
      refetchQueries: [{ query: GET_ALL_BEERS_FILTERED_QUERY }, { query: GET_FAVORITES_COUNT_QUERY }]
    });

  useEffect(() => {
    if (shouldFetch) {
      if (favorited) { // User HAS favorited
        removeFavorite()
      } else { // User HAS NOT favorited
        createFavorite()
      }
    }
    setShouldFetch(false);
  }, [shouldFetch]);

  return <HeartSymbol favorited={favorited} onClick={() => setShouldFetch(true)} />
}

export default AddFavorite;