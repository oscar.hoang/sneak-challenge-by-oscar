import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
import InfiniteScroll from 'react-infinite-scroll-component';

import { GET_ALL_BEERS_FILTERED_QUERY } from '../../query/queries';

import BeerListItem from './BeerListItem';

import EmptyStateBeerList from '../empty-state/EmptyStateBeerList';
import EmptyStateBeerItem from '../empty-state/EmptyStateBeerItem';
import EmptyStateError from '../empty-state/EmptyStateError';

import Spinner from '../utilities/Spinner';

import { ListContainer, CenterContent } from '../styles';

interface Props {
  filter: {
    style_id: string
    country_id: string
  }
  nextPage: Function
}

const BeerList: React.FC<Props> = ({ filter, nextPage }) => {
  const [hasMore, setHasMore] = useState<boolean>(true);
  const [currentPage, setCurrentPage] = useState<number>(0);
  const [itemsPerPage, setItemsPerPage] = useState<number>(10);

  useEffect(() => {
    setCurrentPage(0);
    setHasMore(true);
  }, [filter]);

  const { loading, error, data, fetchMore, networkStatus } = useQuery(GET_ALL_BEERS_FILTERED_QUERY,
    {
      variables:
      {
        page: currentPage,
        perPage: itemsPerPage,
        sortField: "id",
        sortOrder: "ASC",
        ...filter.style_id !== "" && { style_id: filter.style_id },
        ...filter.country_id !== "" && { country_id: filter.country_id }
      },
      notifyOnNetworkStatusChange: true,
      fetchPolicy: "cache-and-network"
    }
  );

  const loadBeers = () => fetchMore({
    query: GET_ALL_BEERS_FILTERED_QUERY,
    variables: {
      page: currentPage,
      perPage: itemsPerPage,
      sortField: "id",
      sortOrder: "DESC",
      ...filter.style_id !== "" && { style_id: filter.style_id },
      ...filter.country_id !== "" && { country_id: filter.country_id }
    },
    updateQuery: (previousResult: any, { fetchMoreResult }) => {
      if (!fetchMoreResult) { 
        setHasMore(false);
        return previousResult;
      }

      if (previousResult && previousResult.allBeers.length === 0) {
        previousResult = data;
        setHasMore(false);
      }
      const seen = new Set();

      return Object.assign({}, previousResult, {
        allBeers: [...previousResult.allBeers, ...fetchMoreResult.allBeers].filter((el: any) => { // Filter for duplicates
          const duplicate = seen.has(el.id);
          seen.add(el.id);
          return !duplicate;
        })
      });
    }
  });

  if (error || !data) return <EmptyStateError textAlign="center" />;

  if (loading || networkStatus === 3) {
    return <CenterContent><Spinner isLarge /></CenterContent>;
  } else {
    if (data && data.allBeers.length > 0) {
      return (
        <ListContainer>
          <InfiniteScroll
            dataLength={data.allBeers.length}
            next={() => {
              setCurrentPage(prevState => prevState + 1);
              loadBeers();
            }}
            hasMore={hasMore}
            className="beer-list"
            loader={<h3>Loading...</h3>}
            endMessage={<EmptyStateBeerItem />}>
            {(data.allBeers || []).map((beer: any, index: number) => <BeerListItem key={index} beer={beer} nextPage={nextPage} />)}
          </InfiniteScroll>
        </ListContainer>
      )
    }
    else {
      return <EmptyStateBeerList />
    }
  }
}

export default BeerList;