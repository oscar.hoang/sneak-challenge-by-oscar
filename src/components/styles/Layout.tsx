import styled from "styled-components";

export const ListContainer = styled.div`
  .beer-list {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 32px;
    grid-row-gap: 32px;
    align-items: center;
    justify-content: center;
    padding: 16px;
    margin: 0 auto;
    @media (max-width: 768px) {
      grid-template-columns: 1fr;
      grid-row-gap: 32px;
    }
    div {
      display: flex;
      flex-direction: column;
    }
  }
`;

export const OverlapContentContainer = styled.div`
  position: relative;
  display: inline-block;
`

export const CenterContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  width: 100%;
  min-height: 200px;
  text-align: center;
`

export const Card = styled.div`
  /* box-shadow: rgba(0, 0, 0, 0.2) 3px 4px 5px; */
  border-radius: 8px;
  overflow: hidden;
  width: 100%;

  &.border {
    border: 1px solid #e5eafa;
  }
  .padding {
    margin: 24px;
  }
  img {
    width: 100%;
    background-size: cover;
    background-position: center center;
    border-radius 8px;
  }
  h2, h3 {
    font-family: 'Roboto Condensed', sans-serif;
    font-weight: 700;
    margin: 8px 0 0 0;
    @media (max-width: 768px) {
      margin-top: 16px;
      font-size: 32px;
    }
  }
  p {
    font-family: 'Source Sans Pro', sans-serif;
    font-weight: 400;
    margin: 0;
  }
`