import React from 'react';
import styled, { keyframes } from "styled-components";

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

interface Props {
  isLarge: boolean
}

const SpinnerStyled = styled.span<{ isLarge: boolean }>`
  animation: ${rotate360} 1s linear infinite;
  transform: translateZ(0);
  
  border-top: 2px solid grey;
  border-right: 2px solid grey;
  border-bottom: 2px solid grey;
  border-left: 4px solid black;
  background: transparent;
  width: ${props => props.isLarge ? "100px" : "24px"};
  height: ${props => props.isLarge ? "100px" : "24px"};
  border-radius: 50%;
`;

const Spinner: React.FC<Props> = ({ isLarge = true }) => {
  return <SpinnerStyled isLarge={isLarge}></SpinnerStyled>
}

export default Spinner;