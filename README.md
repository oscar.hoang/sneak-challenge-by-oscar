## Sample Code

### Overview

A simple React based web application.

We have initialed react project for you which:

1. You could start graphql api server with `npm run gql`, and browse gql playground at http://localhost:3001
2. You could start development with `npm run start`
3. Wireframe for home and detail page

There are 2 main pages

- Home page
- Detail Page

#### Home page

- Fetch data from graphql API and showing list of beers
- Allow user to filter beer style and country
- User could click ♡ button for ♥ favorite and ♡ unfavorite the beer which shown on top right of home page
- When click on photo it's will navigate user to beer detail page

#### Detail page

For detail page, This page is for showing beer details

### Wireframe

Home Page
![Home page](images/home-page.png)

Detail Page
![Detail page](images/detail-page.png)
