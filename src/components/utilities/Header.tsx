import React from 'react'
import styled from 'styled-components';

import FavoritesCount from './FavoritesCount';

import FilterForm from '../form/FilterForm';

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  align-items: center;
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
    padding: 16px;
  }
`

const HeaderContent = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  align-items: center;
  @media (max-width: 768px) {
    grid-row-gap: 32px;
    grid-template-columns: 1fr;
  }
  `

const HeaderStyled = styled.h2`
  margin: 0 0 16px 0;
  font-family: 'Roboto Condensed', sans-serif;
  font-weight: 700;
  font-size: 32px;
  line-height: 52px;
  color: #353535;
  cursor: pointer;
`

interface Props {
  page: number
  selectStyle: string
  selectCountry: string
  previousPage: Function
  handleFormSelectStyle: Function
  handleFormSelectCountry: Function
}

const Header: React.FC<Props> = ({ page, selectStyle, selectCountry, previousPage, handleFormSelectStyle, handleFormSelectCountry }) =>
  <Container>
    <HeaderStyled onClick={() => previousPage()}>Beerify</HeaderStyled>
    {page !== 2 &&
      <>
        <HeaderContent>
          <FilterForm 
            selectStyle={selectStyle}
            selectCountry={selectCountry}
            handleFormSelectStyle={handleFormSelectStyle}
            handleFormSelectCountry={handleFormSelectCountry} />
          <FavoritesCount />
        </HeaderContent>
        <h3>List of Beers</h3>
      </>
    }
  </Container>

export default Header;