import React from 'react';
import { Card } from '../styles';

const EmptyStateBeer: React.FC = () =>
  <Card className="border">
    <div className="padding">
      <h3 style={{ textAlign: 'center' }}>
        That's all we found. Cheers! 🍺🍺🍺
      </h3>
    </div>
  </Card>

export default EmptyStateBeer;