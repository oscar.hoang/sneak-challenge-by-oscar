import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import styled from 'styled-components';
import { GET_BEER_QUERY } from '../../query/queries';

import Breadcrumbs from './Breadcrumbs';
import Favorite from '../utilities/Favorite';
import Image from '../utilities/Image';
import Spinner from '../utilities/Spinner';
import EmptyStateError from '../empty-state/EmptyStateError';

import { CenterContent, OverlapContentContainer, Card } from '../styles'

const Layout = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 30px;
  align-items: center;
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
    padding: 16px;
  }
`

const TextStyled = styled.p`
  font-family: 'Source Sans Pro', sans-serif;
  font-weight: 400;
  font-size: 18px;
  line-height: 26px;
  color: #333333;
  text-align: left;
  &.mb {
    margin-bottom: 42px!important;
  }
  & span {
    font-weight: 700;
  }
`

interface Props {
  beerID: string,
  previousPage: Function
  handleSelectStyle: Function
}

const DetailPage: React.FC<Props> = ({ beerID, previousPage, handleSelectStyle }) => {
  const { loading, error, data } = useQuery(GET_BEER_QUERY,{ variables: { id: beerID } });

  if (error || !data) return <EmptyStateError textAlign="center"/>

  if (loading) return <Spinner isLarge />

  else {
    return (
      <CenterContent>
        <Breadcrumbs
          beer={data.Beer}
          previousPage={previousPage}
          handleSelectStyle={handleSelectStyle} />
        <Card>
          <Layout>
            <OverlapContentContainer>
              <Favorite
                beerID={data.Beer.id}
                favorited={data.Beer.FavoriteBeers.length > 0 ? true : false} />
              <Image 
                beer={data.Beer}
                isClickable={false}
                nextPage={() => { }} />
            </OverlapContentContainer>
            <div>
              <TextStyled><span>Name:</span> {data.Beer.title}</TextStyled>
              <TextStyled><span>Style:</span> {data.Beer.Style.name}</TextStyled>
              <TextStyled className="mb"><span>Country:</span> {data.Beer.Country.name}</TextStyled>
              <TextStyled>{data.Beer.description}</TextStyled>
            </div>
          </Layout>
        </Card>
      </CenterContent>
    )
  }
}

export default DetailPage;