import gql from 'graphql-tag';

export const CREATE_FAVORITE_MUTATION = gql`
  mutation CreateFavoriteBeers($id: ID!, $beer_id: ID!) {
    createFavoriteBeer(id: $id, beer_id: $beer_id) {
      Beer {
        title
      }
    }
  }
`

export const REMOVE_FAVORITE_MUTATION = gql`
  mutation RemoveSelectedFavoriteBeer($id: ID!) {
    removeFavoriteBeer(id: $id)
  }
`