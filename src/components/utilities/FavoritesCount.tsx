import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import styled from 'styled-components';

import { GET_FAVORITES_COUNT_QUERY } from '../../query/queries';

import EmptyStateError from '../empty-state/EmptyStateError';
import Spinner from './Spinner';

const Container = styled.div`
  display: block;
  margin-left: auto;
  h3 { margin: 0; }
`

const FavoritesCount: React.FC = () => {
  const { loading, error, data } = useQuery(GET_FAVORITES_COUNT_QUERY);

  if (error || !data) return <EmptyStateError textAlign="right" />

  if (loading) return <Spinner isLarge={false} />

  return (
    <Container>
      <h3>You have {data._allFavoriteBeersMeta.count} favorite beers</h3>
    </Container>
  )
}

export default FavoritesCount