import React from 'react';
import styled from 'styled-components';

const TextStyled = styled.p<{ textAlign: string }>`
  color: crimson;
  font-weight: bold;
  text-align: ${ props => props.textAlign && props.textAlign};
`

interface Props {
  textAlign: string
}

const EmptyStateError: React.FC<Props> = ({ textAlign }) =>
  <TextStyled textAlign={textAlign}>
    <b>Opps something went wrong.</b>
  </TextStyled>

export default EmptyStateError;