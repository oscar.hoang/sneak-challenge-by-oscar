import React from 'react'
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  margin-bottom: 24px;
  @media (max-width: 768px) {
    width: auto;
    padding: 16px;
  }
`;

const TextStyled = styled.div`
  font-family: 'Source sans pro', sans-serif;
  font-weight: 400;
  font-size: 18px;
  margin-right: 8px;
  color: #333333;
  &.isLink {
    transition: all 0.3s ease;
    cursor: pointer;
    &:hover {
      color: #3f20ba;
    }
  }
`

interface Props {
  beer: {
    title: string,
    Style: {
      id: string
      name: string
    }
  }
  previousPage: Function
  handleSelectStyle: Function
}

const Breadcrumbs: React.FC<Props> = ({ beer, previousPage, handleSelectStyle }) => {
  const { Style: { id, name } } = beer;
  return (
    <Container>
      <TextStyled className="isLink" onClick={() => previousPage()}>Beerify</TextStyled>
      <TextStyled> / </TextStyled>
      <TextStyled className="isLink" onClick={(e) => handleSelectStyle(e, id)}>{name}</TextStyled>
      <TextStyled> / </TextStyled>
      <TextStyled>{beer && beer.title}</TextStyled>
    </Container>
  )
}

export default Breadcrumbs;