import React from 'react'
import { useQuery } from '@apollo/react-hooks';
import styled from 'styled-components';

import { GET_ALL_STYLE_COUNTRY_QUERY } from '../../query/queries';

import Spinner from '../utilities/Spinner';
import EmptyStateError from '../empty-state/EmptyStateError';

import { SelectStyled } from '../styles';

const SelectContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 8px;
  align-items: center;
  @media (max-width: 768px) {
    grid-row-gap: 32px;
    grid-template-columns: 1fr;
  }
`

interface Props {
  selectStyle: string
  selectCountry: string
  handleFormSelectStyle: Function
  handleFormSelectCountry: Function
}

const FilterForm: React.FC<Props> = ({ selectStyle, selectCountry, handleFormSelectStyle, handleFormSelectCountry }) => {
  const { loading, error, data } = useQuery(GET_ALL_STYLE_COUNTRY_QUERY);

  if (loading) return <Spinner isLarge={false} />

  if (error || !data) return <EmptyStateError textAlign="right" />

  return (
    <SelectContainer>
      <SelectStyled
        name="style"
        value={selectStyle}
        onChange={e => handleFormSelectStyle(e)}>
        <option value="" defaultValue="">Style Filter (Default)</option>
        {(data.allStyles || []).map((style: any, i: number) => <option value={style.id} key={i}>{style.name}</option>)}
      </SelectStyled>
      <SelectStyled
        name="country"
        value={selectCountry}
        onChange={e => handleFormSelectCountry(e)}>
        <option value="" defaultValue="">Country Filter (Default)</option>
        {(data.allCountries || []).map((country: any, i: number) => <option value={country.id} key={i}>{country.name}</option>)}
      </SelectStyled>
    </SelectContainer>
  )
}

export default FilterForm;