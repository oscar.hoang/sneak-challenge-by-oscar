import React from 'react';

interface Props {
  beer: {
    [key:string]: any
  }
  isClickable: Boolean
  nextPage: Function
}

const Image: React.FC<Props> = ({ beer, isClickable, nextPage }) =>
  <img 
    style={{ cursor: isClickable ? "pointer" : "default" }}
    src={beer.FavoriteBeers.length > 0 ? `https://i.picsum.photos/id/${beer.id+2}/300/300.jpg` : beer.image_path}
    alt={`${beer.title}`}
    onClick={() => nextPage(beer.id)} />

export default Image;