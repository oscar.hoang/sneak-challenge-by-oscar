import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

import Header from '../utilities/Header';
import BeerList from './BeerList';
import DetailPage from '../detail/DetailPage';

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 50px auto;
  width: 100%;
  max-width: 980px;
  @media (max-width: 768px) {
    margin: 0 auto;
  }
`

interface FilterState {
  style_id: string
  country_id: string
}

const Homepage: React.FC = () => {
  const [page, setPage] = useState<number>(1);
  const [beerID, setBeerID] = useState<string>("");
  const [filter, setFilter] = useState<FilterState>({style_id: "", country_id: ""});
  const [selectStyle, setSelectStyle] = useState<string>("");
  const [selectCountry, setSelectCountry] = useState<string>("");

  useEffect(() => {
    setFilter({
      style_id: selectStyle,
      country_id: selectCountry
    });
  }, [selectStyle, selectCountry])

  const nextPage = (selectedID: string) => {
    setPage(page + 1);
    setBeerID(selectedID);
  }

  const previousPage = () => {
    setPage(1);
  }

  const handleSelectStyle = (e: React.ChangeEvent<HTMLInputElement>, id: string) => {
    setPage(1);
    setSelectStyle(id);
  }

  const handleFormSelectStyle = (e: React.ChangeEvent<HTMLSelectElement>) => {
    let value = e.target.value;
    setSelectStyle(value);
  }

  const handleFormSelectCountry = (e: React.ChangeEvent<HTMLSelectElement>) => {
    let value = e.target.value;
    setSelectCountry(value);
  }

  return (
    <MainContainer>
      <Header 
        page={page}
        previousPage={previousPage}
        selectStyle={selectStyle}
        selectCountry={selectCountry}
        handleFormSelectStyle={handleFormSelectStyle}
        handleFormSelectCountry={handleFormSelectCountry}/>
      {page === 1 ? 
        <BeerList 
          filter={filter}
          nextPage={nextPage} />
        :
        <DetailPage
          beerID={beerID}
          previousPage={previousPage}
          handleSelectStyle={handleSelectStyle} />
      }
    </MainContainer>
  )
}

export default Homepage;